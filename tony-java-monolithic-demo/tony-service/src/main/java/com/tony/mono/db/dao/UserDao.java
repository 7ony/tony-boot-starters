package com.tony.mono.db.dao;

import com.tony.mono.db.po.User;
import com.tony.mybatis.dao.BaseDao;

public interface UserDao extends BaseDao<User> {

}
