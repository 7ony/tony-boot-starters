package com.tony.demo.sys.dao

import com.tony.demo.sys.po.Dict
import com.tony.mybatis.dao.BaseDao

interface DictDao : BaseDao<Dict>
