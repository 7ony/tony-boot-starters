package com.tony.demo.sys.dao

import com.tony.demo.sys.po.DictType
import com.tony.mybatis.dao.BaseDao

interface DictTypeDao : BaseDao<DictType>
