dependencies {
    api(projects.tonyCore)
    implementation(tonyLibs.springBoot)
    implementation("com.alipay.sdk:alipay-sdk-java:4.39.158.ALL")
}
