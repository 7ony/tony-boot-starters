# 基础库
## build-script 编译脚本依赖, 可编写自定义gradle插件.
## tony-aliyun-oss 阿里云oss starter
## tony-aliyun-sms 阿里云短信 starter
## tony-cache redis缓存封装, 及注解式redis缓存,实现了参数化过期
## tony-captcha 简易验证码 starter
## tony-core 常用工具封装
## tony-dependencies 依赖pom
## tony-dependencies-catalog libs.versions.toml 依赖
## tony-feign open-feign starter
## tony-jwt jwt starter
## tony-knife4j-api knife4j 接口文档starter
## tony-mybatis-plus mybatis-plus 做了一些扩展
## tony-snowflake-id 雪花id
## tony-web 常用web封装. 统一异常处理, 响应结构及行为统一等.
## tony-web-auth web认证
## tony-wechat 微信接口 starter
