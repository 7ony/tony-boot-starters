package com.tony.fus.db.mapper

import com.tony.fus.db.po.FusExtInstance
import com.tony.mybatis.dao.BaseDao

/**
 * 扩展流程实例 Mapper
 * @author tangli
 * @date 2024/01/24 10:58
 * @since 1.0.0
 */
public interface FusExtInstanceMapper : BaseDao<FusExtInstance>
