-- mysql 8.0.13+ json default value supported
DROP TABLE IF EXISTS `fus_task_actor` CASCADE;
DROP TABLE IF EXISTS `fus_task` CASCADE;
DROP TABLE IF EXISTS `fus_instance` CASCADE;
DROP TABLE IF EXISTS `fus_ext_instance` CASCADE;
DROP TABLE IF EXISTS `fus_history_task_actor` CASCADE;
DROP TABLE IF EXISTS `fus_history_task` CASCADE;
DROP TABLE IF EXISTS `fus_ext_instance` CASCADE;
DROP TABLE IF EXISTS `fus_history_instance` CASCADE;
DROP TABLE IF EXISTS `fus_process` CASCADE;

CREATE TABLE `fus_process`
(
    `process_id`      varchar(32)  NOT NULL COMMENT '主键ID',
    `tenant_id`       varchar(32)  NOT NULL DEFAULT '' COMMENT '租户ID',
    `creator_id`      varchar(32)  NOT NULL DEFAULT '' COMMENT '创建人ID',
    `creator_name`    varchar(50)  NOT NULL DEFAULT '' COMMENT '创建人',
    `create_time`     timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `process_name`    varchar(100) NOT NULL DEFAULT '' COMMENT '流程名称',
    `process_key`     varchar(200) NOT NULL DEFAULT '' COMMENT '流程定义 key 唯一标识',
    `process_type`    varchar(100) NOT NULL DEFAULT '' COMMENT '流程类型',
    `process_version` int          NOT NULL DEFAULT '1' COMMENT '流程版本，默认 1',
    `remark`          varchar(255) NOT NULL DEFAULT '' COMMENT '备注说明',
    `enabled`         tinyint      NOT NULL DEFAULT '1' COMMENT '流程状态: 0.不可用, 1.可用',
    `model_content`   json         NOT NULL DEFAULT ('{}') COMMENT '流程模型定义JSON内容',
    `sort`            smallint     NOT NULL DEFAULT '0' COMMENT '排序',
    PRIMARY KEY (`process_id`) USING BTREE,
    KEY `idx_process_name` (`process_name`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = DYNAMIC COMMENT ='流程定义表';

CREATE TABLE `fus_instance`
(
    `instance_id`        varchar(32)  NOT NULL COMMENT '主键ID',
    `tenant_id`          varchar(32)  NOT NULL DEFAULT '' COMMENT '租户ID',
    `creator_id`         varchar(32)  NOT NULL DEFAULT '' COMMENT '创建人ID',
    `creator_name`       varchar(50)  NOT NULL DEFAULT '' COMMENT '创建人',
    `create_time`        timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `process_id`         varchar(32)  NOT NULL DEFAULT '' COMMENT '流程定义ID',
    `parent_instance_id` varchar(32)  NOT NULL DEFAULT '' COMMENT '父流程实例ID',
    `business_key`       varchar(100) NOT NULL DEFAULT '' COMMENT '业务KEY',
    `variable`           json         NOT NULL DEFAULT ('{}') COMMENT '变量json',
    `node_name`          varchar(100) NOT NULL DEFAULT '' COMMENT '节点名称',
    `expire_time`        timestamp    NULL     DEFAULT NULL COMMENT '期望完成时间',
    `updator_id`         varchar(32)  NOT NULL DEFAULT '' COMMENT '更新人id',
    `updator_name`       varchar(50)  NOT NULL DEFAULT '' COMMENT '上次更新人',
    `update_time`        timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '上次更新时间',
    PRIMARY KEY (`instance_id`) USING BTREE,
    KEY `idx_instance_process_id` (`process_id`) USING BTREE,
    CONSTRAINT `fk_instance_process_id` FOREIGN KEY (`process_id`) REFERENCES `fus_process` (`process_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = DYNAMIC COMMENT ='流程实例表';

CREATE TABLE `fus_task`
(
    `task_id`        varchar(32)  NOT NULL COMMENT '主键ID',
    `tenant_id`      varchar(32)  NOT NULL DEFAULT '' COMMENT '租户ID',
    `creator_id`     varchar(32)  NOT NULL DEFAULT '' COMMENT '创建人ID',
    `creator_name`   varchar(50)  NOT NULL DEFAULT '' COMMENT '创建人',
    `create_time`    timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `instance_id`    varchar(32)  NOT NULL DEFAULT '' COMMENT '流程实例ID',
    `parent_task_id` varchar(32)  NOT NULL DEFAULT '' COMMENT '父任务ID',
    `task_name`      varchar(100) NOT NULL DEFAULT '' COMMENT '任务名称',
    `task_type`      smallint     NOT NULL COMMENT '任务类型: 1.主办, 2.审批, 3.抄送, 4.条件审批, 5.条件分支, 6.子流程, 7.定时器, 8.触发器, 11.转办, 12.委派, 13.委派归还',
    `perform_type`   smallint     NOT NULL COMMENT '参与类型: 1.发起、其它, 2.按顺序依次审批, 3.会签, 4.或签, 5.票签, 10.抄送',
    `variable`       json         NOT NULL DEFAULT ('{}') COMMENT '变量json',
    `assignor_id`    varchar(32)  NOT NULL DEFAULT '' COMMENT '委托人ID',
    `assignor_name`  varchar(100) NOT NULL DEFAULT '' COMMENT '委托人',
    `expire_time`    timestamp    NULL     DEFAULT NULL COMMENT '任务期望完成时间',
    `remind_time`    timestamp    NULL     DEFAULT NULL COMMENT '提醒时间',
    `remind_repeat`  smallint     NOT NULL DEFAULT '0' COMMENT '提醒次数',
    `viewed`         tinyint      NOT NULL DEFAULT '0' COMMENT '已阅: 0.否, 1.是',
    PRIMARY KEY (`task_id`) USING BTREE,
    KEY `idx_task_instance_id` (`instance_id`) USING BTREE,
    CONSTRAINT `fk_task_instance_id` FOREIGN KEY (`instance_id`) REFERENCES `fus_instance` (`instance_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = DYNAMIC COMMENT ='任务表';

CREATE TABLE `fus_task_actor`
(
    `task_actor_id` varchar(32)  NOT NULL COMMENT '主键 ID',
    `tenant_id`     varchar(32)  NOT NULL DEFAULT '' COMMENT '租户ID',
    `instance_id`   varchar(32)  NOT NULL DEFAULT '' COMMENT '流程实例ID',
    `task_id`       varchar(32)  NOT NULL DEFAULT '' COMMENT '任务ID',
    `actor_id`      varchar(32)  NOT NULL DEFAULT '' COMMENT '参与者ID',
    `actor_name`    varchar(100) NOT NULL DEFAULT '' COMMENT '参与者名称',
    `actor_type`    int          NOT NULL COMMENT '参与者类型: 1.用户, 2.角色, 3.部门',
    `weight`        int          NOT NULL DEFAULT '0' NULL COMMENT '票签权重',
    PRIMARY KEY (`task_actor_id`) USING BTREE,
    KEY `idx_task_actor_task_id` (`task_id`) USING BTREE,
    CONSTRAINT `fk_task_actor_task_id` FOREIGN KEY (`task_id`) REFERENCES `fus_task` (`task_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = DYNAMIC COMMENT ='任务参与者表';

CREATE TABLE `fus_history_instance`
(
    `instance_id`        varchar(32)  NOT NULL COMMENT '主键ID',
    `tenant_id`          varchar(32)  NOT NULL DEFAULT '' COMMENT '租户ID',
    `creator_id`         varchar(32)  NOT NULL DEFAULT '' COMMENT '创建人ID',
    `creator_name`       varchar(50)  NOT NULL COMMENT '创建人',
    `create_time`        timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `process_id`         varchar(32)  NOT NULL DEFAULT '' COMMENT '流程定义ID',
    `parent_instance_id` varchar(32)  NOT NULL DEFAULT '' COMMENT '父流程实例ID',
    `business_key`       varchar(100) NOT NULL DEFAULT '' COMMENT '业务KEY',
    `variable`           json         NOT NULL DEFAULT ('{}') COMMENT '变量json',
    `node_name`          varchar(100) NOT NULL DEFAULT '' COMMENT '节点名称',
    `expire_time`        timestamp    NULL     DEFAULT NULL COMMENT '期望完成时间',
    `updator_id`         varchar(32)  NOT NULL DEFAULT '' COMMENT '更新人id',
    `updator_name`       varchar(50)  NOT NULL DEFAULT '' COMMENT '上次更新人',
    `update_time`        timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '上次更新时间',
    `instance_state`     smallint     NOT NULL DEFAULT '1' COMMENT '流程实例状态: 1.审批中, 2.审批通过, 3.审批拒绝, 4.撤销审批, 5.超时结束, 6.强制终止',
    `end_time`           timestamp    NULL     DEFAULT NULL COMMENT '结束时间',
    `duration`           bigint       NULL COMMENT '处理耗时',
    PRIMARY KEY (`instance_id`) USING BTREE,
    KEY `idx_his_instance_process_id` (`process_id`) USING BTREE,
    CONSTRAINT `fk_his_instance_process_id` FOREIGN KEY (`process_id`) REFERENCES `fus_process` (`process_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = DYNAMIC COMMENT ='历史流程实例表';

CREATE TABLE `fus_ext_instance`
(
    `instance_id`   varchar(32) NOT NULL COMMENT '主键ID',
    `tenant_id`     varchar(32) NOT NULL DEFAULT '' COMMENT '租户ID',
    `process_id`    varchar(32) NOT NULL DEFAULT '' COMMENT '流程定义ID',
    `model_content` json        NOT NULL DEFAULT ('{}') COMMENT '流程模型定义JSON内容',
    PRIMARY KEY (`instance_id`) USING BTREE,
    CONSTRAINT `fk_ext_instance_id` FOREIGN KEY (`instance_id`) REFERENCES `fus_history_instance` (`instance_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = Dynamic COMMENT = '扩展流程实例表';

CREATE TABLE `fus_history_task`
(
    `task_id`         varchar(32)  NOT NULL COMMENT '主键ID',
    `tenant_id`       varchar(32)  NOT NULL DEFAULT '' COMMENT '租户ID',
    `creator_id`      varchar(32)  NOT NULL DEFAULT '' COMMENT '创建人ID',
    `creator_name`    varchar(50)  NOT NULL COMMENT '创建人',
    `create_time`     timestamp    NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `instance_id`     varchar(32)  NOT NULL DEFAULT '' COMMENT '流程实例ID',
    `parent_task_id`  varchar(32)  NOT NULL DEFAULT '' COMMENT '父任务ID',
    `out_process_id`  varchar(32)  NOT NULL DEFAULT '' COMMENT '外部流程定义ID',
    `out_instance_id` varchar(32)  NOT NULL DEFAULT '' COMMENT '外部流程实例ID',
    `task_name`       varchar(100) NOT NULL COMMENT '任务名称',
    `task_type`       smallint     NOT NULL COMMENT '任务类型: 1.主办, 2.审批, 3.抄送, 4.条件审批, 5.条件分支, 6.子流程, 7.定时器, 8.触发器, 11.转办, 12.委派, 13.委派归还',
    `perform_type`    smallint     NOT NULL COMMENT '参与类型: 1.发起、其它, 2.按顺序依次审批, 3.会签, 4.或签, 5.票签, 10.抄送',
    `variable`        json         NOT NULL DEFAULT ('{}') COMMENT '变量json',
    `assignor_id`     varchar(32)  NOT NULL DEFAULT '' COMMENT '委托人ID',
    `assignor_name`   varchar(100) NOT NULL DEFAULT '' COMMENT '委托人',
    `expire_time`     timestamp    NULL     DEFAULT NULL COMMENT '任务期望完成时间',
    `remind_time`     timestamp    NULL     DEFAULT NULL COMMENT '提醒时间',
    `remind_repeat`   smallint     NOT NULL DEFAULT '0' COMMENT '提醒次数',
    `viewed`          tinyint      NOT NULL DEFAULT '0' COMMENT '已阅: 0.否, 1.是',
    `end_time`        timestamp    NULL     DEFAULT NULL COMMENT '任务结束时间',
    `task_state`      smallint     NOT NULL DEFAULT '1' COMMENT '任务状态: 1.活动, 2.跳转, 3.完成, 4.拒绝, 5.撤销, 6.超时, 7.终止, 8.驳回终止',
    `duration`        bigint       NULL COMMENT '处理耗时',
    PRIMARY KEY (`task_id`) USING BTREE,
    KEY `idx_his_task_instance_id` (`instance_id`) USING BTREE,
    KEY `idx_his_task_parent_task_id` (`parent_task_id`) USING BTREE,
    CONSTRAINT `fk_his_task_instance_id` FOREIGN KEY (`instance_id`) REFERENCES `fus_history_instance` (`instance_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = DYNAMIC COMMENT ='历史任务表';

CREATE TABLE `fus_history_task_actor`
(
    `task_actor_id` varchar(32)  NOT NULL COMMENT '主键 ID',
    `tenant_id`     varchar(32)  NOT NULL DEFAULT '' COMMENT '租户ID',
    `instance_id`   varchar(32)  NOT NULL DEFAULT '' COMMENT '流程实例ID',
    `task_id`       varchar(32)  NOT NULL DEFAULT '' COMMENT '任务ID',
    `actor_id`      varchar(32)  NOT NULL DEFAULT '' COMMENT '参与者ID',
    `actor_name`    varchar(100) NOT NULL DEFAULT '' COMMENT '参与者名称',
    `actor_type`    int          NOT NULL COMMENT '参与者类型: 1.用户, 2.角色, 3.部门',
    `weight`        int          NULL COMMENT '票签权重',
    PRIMARY KEY (`task_actor_id`) USING BTREE,
    KEY `idx_his_task_actor_task_id` (`task_id`) USING BTREE,
    CONSTRAINT `fk_his_task_actor_task_id` FOREIGN KEY (`task_id`) REFERENCES `fus_history_task` (`task_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_general_ci
  ROW_FORMAT = DYNAMIC COMMENT ='历史任务参与者表';
